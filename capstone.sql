USE classic_models;

-- 1 
SELECT * FROM customers WHERE country = "Philippines";

-- 2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5 
SELECT customerName FROM customers WHERE state IS NULL;

-- 6 
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7 
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9
SELECT textDescription FROM productLines WHERE textDescription LIKE "%state of the art%";

-- 10
SELECT DISTINCT country FROM customers;  

-- 11
SELECT DISTINCT status FROM orders;

-- 12 
SELECT customerName, country FROM customers WHERE country IN ("USA", "FRANCE", "CANADA");

-- 13
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE offices.city = "Tokyo";

-- 14
SELECT customerName  FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE lastName = "Thompson" AND firstName = "Leslie";

-- 15 Return the product names and customer name of products ordered by "Baane Mini Sports"
SELECT products.productName, customers.customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customers.customerName = "Baane Mini Imports";

-- 16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE  customers.country = offices.country;

-- 17 
SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;



-- 18
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%"